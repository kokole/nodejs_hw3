const secret = process.env.SECRET;
const User = require('../models/User');

let jwt = require('jsonwebtoken');
let bcrypt = require('bcryptjs');

exports.register = (req, res) => {
  const user = new User({
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password),
    role: req.body.role,
  });

  user.save((err, user) => {
    if (err) {
      res.status(500).send({message: 'Error message'})
      return;
    }

    return res.status(200).json({message: 'Profile created successfully'})
  });
}

exports.login = (req, res) => {
  User.findOne({
    email: req.body.email
  })
    .exec((err, user) => {
      if (err) {
        res.status(500).send({message: 'Error message'})
        return
      }

      if (!user) {
        return res.status(400).send({message: 'User Not found'});
      }

      let passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(400).send({
          message: 'Invalid Password!'
        });
      }

      let token = jwt.sign({id: user.id, role: user.role}, secret, {
        expiresIn: 86400 //24 hours
      });

      res.status(200).send({
        jwt_token: token
      })
    })
}

