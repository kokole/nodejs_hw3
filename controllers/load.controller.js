const Load = require('../models/Load');
const Truck = require('../models/Truck');

const truckTypes = [
  {
    type: 'SPRINTER',
    length: 300,
    width: 250,
    height: 170,
    weight: 1700
  },

  {
    type: 'SMALL STRAIGHT',
    length: 500,
    width: 250,
    height: 170,
    weight: 2500
  },

  {
    type: 'LARGE STRAIGHT',
    length: 700,
    width: 350,
    height: 200,
    weight: 4000
  },
]


exports.addLoad = (req, res) => {
  const newLoad = new Load ({
    created_by: req.userId,
    status: 'NEW',
    name: req.body.name,
    payload: req.body.payload,
    pickup_address: req.body.pickup_address,
    delivery_address: req.body.delivery_address,
    dimensions: {
      width: req.body.dimensions.width,
      length: req.body.dimensions.length,
      height: req.body.dimensions.height
    }
  });

  newLoad.save((err, load) => {
    if (err) {
      res.status(500).send('Error message');
      return;
    }

    if (!load) {
      res.status(400).send({message: 'Error message'});
      return;
    };

    return res.status(200).send({message: 'Load created successfully'})
  })
}

exports.getLoads = (req,res) => {
  if (req.role === 'SHIPPER') {
    Load.find(
      { 
        created_by: req.userId,
        status: req.query.status || { $in: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED']},
      },
      null,
      {
        limit: parseInt(req.query.limit, 10) || 10,
        offset: parseInt(req.query.offset, 10) || 0
      }
    )
    .exec((err, loads) => {
      if (err) {
        res.status(500).send({message: 'Error message'});
        return;
      };

      if (!loads) {
        res.status(400).send({message: 'Smth went wrong'})
        return;
      }

      return res.status(200).send({
        loads: loads
      });
    })
  } else if (req.role === 'DRIVER') {
    Load.find(
      { 
        assigned_to: req.userId,
        status: {$exists: req.query.status, $ne: null},
        // state: req.role === 'SHIPPER' ? {$exists: false} : {$exists: true}
      },
      null,
      {
        limit: parseInt(req.query.limit, 10) || 10,
        offset: parseInt(req.query.offset, 10) || 0
      }
    )
    .exec((err, loads) => {
      if (err) {
        res.status(500).send({message: 'Error message'});
        return;
      };

      if (!loads) {
        res.status(400).send({message: 'Trucks not founded'})
        return;
      };

      return res.status(200).send({
        loads: loads
      });
    });
  };
}

exports.getActiveLoad = (req,res) => {
  Load.findOne(
    { 
      assigned_to: req.userId,
      state: 'En route to Pick Up' || 'Arrived to Pick Up' || 'En route to delivery',
    },
    null,
    {
      limit: parseInt(req.query.limit, 10) || 10,
      offset: parseInt(req.query.offset, 10) || 0
    }
  )
  .exec((err, load) => {
    if (err) {
      res.status(500).send({message: 'Error message'});
      return;
    };

    if (!load) {
      res.status(200).send({message: 'Load not founded'})
      return;
    }

    return res.status(200).send({load: load});
  })
}

exports.getLoadById = (req, res) => {
  if (req.role = 'SHIPPER') {
    Load.findOne(
      { 
        _id: req.params.id,
        created_by: req.userId
      }
    )
    .exec((err, load) => {
      if (err) {
        res.status(500).send({message: 'Error message'});
        return;
      };

      if (!load) {
        res.status(400).send({message: 'Load not founded'})
        return;
      }

      return res.status(200).send({load: load});
    })
  } else {
    Load.findOne(
      { 
        _id: req.params.id,
        assigned_to: req.userId
      }
    )
    .exec((err, load) => {
      if (err) {
        res.status(500).send({message: 'Error message'});
        return;
      };

      if (!load) {
        res.status(400).send({message: 'Load not founded'})
        return;
      }

      return res.status(200).send({load: load});
    });
  };
}

exports.deleteLoad = (req, res) => {
  Load.findOneAndDelete(
    { 
      _id: req.params.id,
      created_by: req.userId
    }
  )
  .exec((err, load) => {
    if (err) {
      res.status(500).send({message: 'Error message'});
      return;
    };

    if (!load) {
      res.status(400).send({message: 'Load not founded'})
      return;
    }

    return res.status(200).send({message: 'Load deleted successfully'});
  })
}

exports.updateLoad = (req, res) => {
  Load.findOneAndUpdate(
    { 
      _id: req.params.id,
      created_by: req.userId
    },
    req.body,
    {
      new: true
    },
    (err, load) => {
      if (err) {
        res.status(500).send({message: 'Error message'});
        return;
      };

      if (!load) {
        res.status(400).send({message: 'Load not founded'})
        return;
      }

      return res.status(200).send({message: 'Load details changed successfully'});
    }
  );
};

exports.postLoad = (req,res) => {
  Load.findOne(
    {
      _id: req.params.id,
      created_by: req.userId
    }
  )
  .exec((err, load) => {
    if (err) {
      res.status(500).send({message: 'Error message'});
      return;
    }

    if (!load) {
      res.status(400).send({message: 'Load not founded'});
      return;
    }

    load.status = 'POSTED';
    load.save((err, newload) => {
      if (err) {
        res.status(500).send({message: 'Error message'});
        return;
      }
//find assigned trucks with IS status 
      Truck.find(
        {
          assigned_to: {$ne: null},
          status: 'IS',
        }
      )
      .exec((err, availableTrucks) => {
        if (err) {
          res.status(500).send({message: 'Error message: ' + err});
          return;
        };

        if (!availableTrucks) {
          res.status(400).send({message: 'No available trucks'});
          return;
        };
//loop through trucks to check dimentions and payload
        for (truck of availableTrucks) {
          for (truckType of truckTypes) {
            if 
              (truck.type === truckType.type &&
              truckType.length > newload.dimensions.length &&
              truckType.width > newload.dimensions.width &&
              truckType.height > newload.dimensions.height &&
              truckType.weight > newload.payload) {
                //truck is found, lets assign load to it!
                newload.status = 'ASSIGNED';
                newload.state = 'En route to Pick Up';
                newload.assigned_to = truck.assigned_to;
                newload.logs.push({message: 'Load assigned to driver with id ' + truck.assigned_to});
                truck.status = 'OL';
                newload.save((err, postedLoad) => {
                  if (err) {
                    res.status(500).send({message: 'Error with saving new posted load: ' + err})
                    return;
                  };
                });
                truck.save((err, assignedTruck) => {
                  if (err) {
                    res.status(500).send({message: 'Error with saving new assigned truck: ' + err})
                    return;
                  };
                });

                res.status(200).send({message: 'Load posted successfully', driver_found: true})
                break;
              };
          };
        };
        if (newload.status != 'ASSIGNED') {
          return res.status(400).send({message: 'Cant find appropriate truck'});
        };
        //res.status(200).send(availableTrucks);
      }); //exec()
    });
  });
};

exports.getLoadShippingInfo = (req, res) => {
  Load.findOne(
    {
      _id: req.params.id,
      created_by: req.userId
    }
  )
  .exec((err, load) => {
    if (err) {
        res.status(500).send({message: 'Error message'});
        return;
      };

      if (!load) {
        res.status(400).send({message: 'Load not founded'});
        return;
      }

      return res.status(200).send({load: load});
  })
}

exports.nextLoadState = (req,res) => {
  Load.findOne({assigned_to: req.userId, status: 'ASSIGNED'}).exec((err, load) => {
    if (err) {
      res.status(500).send({message: 'Error message'});
      return;
    }

    if(!load) {
      res.status(400).send({message: 'Load not founded'});
      return;
    }

    if (load.state.toLowerCase() == 'en route to pick up') {
      load.state = 'Arrived to Pick Up';
    } else if (load.state.toLowerCase() == 'arrived to pick up') {
      load.state = 'En route to delivery';
    } else if (load.state.toLowerCase() == 'en route to delivery') {
      load.state = 'Arrived to delivery';
      load.status = 'SHIPPED';
    };

    load.save((err, newload) => {
      if (err) {
        res.status(500).send({message: 'Error with changing load state'});
        return;
      }

      if (newload.status == 'SHIPPED') {
        Truck.findOne({assigned_to: req.userId}).exec((err, truck) => {
          if (err) {
            res.status(500).send({message: 'Error message'});
            return;
          }

          truck.status = 'IS';
          truck.save((err, newtruck) => {
            if (err) {
              res.status(500).send({message: 'Error message'});
              return;
            };
          });
        });
      };

      return res.status(200).send({message: `Load state changed to '${newload.state}'`});
    });
  });
};