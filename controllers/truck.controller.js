const Truck = require('../models/Truck');

exports.addTruck = (req, res) => {
  const truck = new Truck({
    created_by: req.userId,
    assigned_to: null,
    type: req.body.type,
    status: 'IS',
  });

  truck.save((err, newTruck) => {
    if (err) {
      res.status(500).send({message: 'Error with saving truck'});
      return;
    }

    if (!newTruck) {
      res.status(400).send({message: 'Error message'});
      return;
    };

    return res.status(200).send({message: 'Truck created successfully'})
  })
}

exports.getTruckById = (req, res) => {
  Truck.findOne(
    {
      created_by: req.userId,
      _id: req.params.id,
    }
  )
  .exec((err, truck) => {
    if (err) {
      res.status(500).send({message: 'Error message'})
      return;
    }

    if (!truck) {
      res.status(400).send({message: 'Truck not found'})
      return;
    }
    res.status(200).send(
      {
        truck: {       
          _id: truck._id,
          created_by: truck.created_by,
          assigned_to: truck.assigned_to,
          type: truck.type,
          status: truck.status,
          created_date: truck.created_date, 
        }
      }
    );
  })
}

exports.updateTruck = (req, res) => {
  Truck.findOneAndUpdate(
    {
      _id: req.params.id,
      created_by: req.userId,
    },
    req.body,
    {
      new: true,
    }
  )
  .exec((err, truck) => {
    if (err) {
      res.status(500).send({message: 'Error message'})
      return;
    };

    if (!truck) {
      res.status(400).send({message: 'Truck not found'})
      return;
    };

    res.status(200).send({message: 'Truck details changed successfully'});
    return;
  })
}

exports.getTrucks = (req, res) => {
  Truck.find(
    {
      created_by: req.userId,
    }
  )
  .exec((err, trucks) => {
    if (err) {
      res.status(500).send({message: 'Error message'})
      return;
    }

    if (!trucks) {
      res.status(400).send({message: 'Trucks not found'})
      return;
    }
    res.status(200).send(
      {
        trucks: trucks
      }
    );
  })
}

exports.deleteTruck = (req, res) => {
  Truck.findOneAndDelete(
    {
      _id: req.params.id,
      created_by: req.userId,
    }
  )
  .exec((err, truck) => {
    if (err) {
      res.status(500).send({message: 'Error message'});
      return;
    }

    if (!truck) {
      res.status(400).send({message: 'User not found'});
      return;
    }

    res.status(200).send({
      message: 'Truck deleted successfully'
    });
  })
}

exports.assignTruck = (req,res) => {
  Truck.findOneAndUpdate(
    {_id: req.params.id},
    {assigned_to: req.userId},
    {new: true},
    ((err,truck) => {
    if(err) {
      res.status(500).send({message: 'Error message'});
      return;
    };

    if(!truck) {
      res.status(400).send({message: 'Truck not found'});
      return;
    };

    res.status(200).send({message: 'Truck assigned successfully'});
    return;
    })
  );
}