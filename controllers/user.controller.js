const User = require('../models/User');
let bcrypt = require('bcryptjs');

exports.getProfile = (req, res) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({message: 'Error message'});
      return;
    }

    if (!user) {
      res.status(400).send({message: 'User not found'});
      return;
    }

    res.status(200).send({
      user: {
        _id: user._id,
        role: user.role,
        email: user.email,
        created_date: user.created_date,
      }
    });
  })
}

exports.deleteProfile = (req, res) => {
  User.deleteOne({userId: req.userId}).exec((err, user) => {
    if (err) {
      res.status(500).send({message: 'Error message'});
      return;
    }

    if (!user) {
      res.status(400).send({message: 'User not found'});
      return;
    }

    res.status(200).send({
      message: 'Profile deleted successfully'
    });
  })
}

exports.changePassword = (req, res) => {
  User.findById(req.userId).exec((err, user) => {
    let passwordIsValid = bcrypt.compareSync(
      req.body.oldPassword,
      user.password
    );
    if (!passwordIsValid) {
      return res.status(400).send({
        message: 'Invalid Old Password!'
      });
    };
    user.password = bcrypt.hashSync(req.body.newPassword);
    user.save((err, user) => {
      if (err) {
        res.status(500).send({message: 'Error message'})
        return;
      }
      return res.status(200).json({message: 'Password changed successfully'})
    });
  })
}