const jwt = require("jsonwebtoken");
const secret = process.env.SECRET;

verifyToken = (req, res, next) => {
  let raw_token = req.headers.authorization;
  let token = raw_token.split(' ')[1];

  if (!token) {
    return res.status(400).send({ message: "No token provided!" });
  };

  jwt.verify(token, secret, (err, decoded) => {
    if (err) {
      return res.status(400).send({ message: "Unauthorized!" });
    }
    req.userId = decoded.id;
    req.role = decoded.role;
    next();
  });
};

isDriver = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({message: err});
      return;
    }
    if (user.role != 'DRIVER') {
      res.status(400).send({message: 'Require Driver Role'})
      return;
    }

    next();
    
  })
}

isShipper = (req, res, next) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      res.status(500).send({message: err});
      return;
    }
    if (user.role != 'SHIPPER') {
      res.status(400).send({message: 'Require Shipper Role'})
      return;
    }

    next();
  })
}

const authJwt = {
  verifyToken,
  isDriver,
  isShipper
};

module.exports = authJwt;
