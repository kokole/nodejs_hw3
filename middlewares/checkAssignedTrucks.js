const Truck = require('../models/Truck');

checkAssignedTrucks = (req, res, next) => {
  Truck.findOne(
    {
      assigned_to: req.userId,
    }
  ).exec((err, truck)=> {
    if (err) {
      res.status(500).send({message: 'Error message'});
      return;
    }

    if (truck) {
      res.status(400).send({message: 'This driver already has assigned truck'});
      return;
    }

    next();
  })
}

module.exports = checkAssignedTrucks;