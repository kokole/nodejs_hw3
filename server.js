//environment variables
require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require("body-parser");

const app = express();
const PORT = process.env.PORT || 8080;

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan('tiny'));

//database connection
const uri = process.env.ATLAS_URI;
mongoose.connect(uri)
	.then(()=>{
		console.log("Database CONNECTED");
	})
	.catch(err=>{
		console.error("Connection error", err)
	})	;

app.listen(PORT, (err) => {
	if (err) console.log(err);
	console.log(`Server running on port ${PORT}`);
})

//routes
require('./routes/auth.routes')(app);
require('./routes/user.routes')(app);
require('./routes/truck.routes')(app);
require('./routes/load.routes')(app);