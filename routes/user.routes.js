const authJwt = require("../middlewares/authJwt");
const controller = require("../controllers/user.controller");

module.exports = function(app) {
  app.get('/api/users/me',
    authJwt.verifyToken,
    controller.getProfile
  );

  app.delete('/api/users/me',
    authJwt.verifyToken,
    controller.deleteProfile
  );

  app.patch('/api/users/me/password',
    authJwt.verifyToken,
    controller.changePassword)
}