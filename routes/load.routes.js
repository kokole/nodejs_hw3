const authJwt = require("../middlewares/authJwt");
const controller = require("../controllers/load.controller");

module.exports = function(app) {
  app.post('/api/loads',
    authJwt.verifyToken,
    authJwt.isShipper,
    controller.addLoad
  );

  app.get('/api/loads',
    authJwt.verifyToken,
    controller.getLoads
  );

  app.get('/api/loads/active',
    authJwt.verifyToken,
    authJwt.isDriver,
    controller.getActiveLoad
  );

  app.get('/api/loads/:id',
    authJwt.verifyToken,
    controller.getLoadById
  );

  app.delete('/api/loads/:id',
    authJwt.verifyToken,
    authJwt.isShipper,
    controller.deleteLoad
  );

  app.put('/api/loads/:id',
    authJwt.verifyToken,
    authJwt.isShipper,
    controller.updateLoad
  );

  app.post('/api/loads/:id/post',
    authJwt.verifyToken,
    authJwt.isShipper,
    controller.postLoad
  );

  app.get('/api/loads/:id/shipping_info',
    authJwt.verifyToken,
    authJwt.isShipper,
    controller.getLoadShippingInfo
  );

  app.patch('/api/loads/active/state',
    authJwt.verifyToken,
    authJwt.isDriver,
    controller.nextLoadState
  );
}