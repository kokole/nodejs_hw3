const verifySignUp = require("../middlewares/verifySignUp");
const controller = require("../controllers/auth.controller");

module.exports = function(app) {
  app.post('/api/auth/register',
    verifySignUp,
    controller.register
  );

  app.post("/api/auth/login", controller.login);
}