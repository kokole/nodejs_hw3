const authJwt = require("../middlewares/authJwt");
const checkAssignedTrucks = require('../middlewares/checkAssignedTrucks');
const controller = require("../controllers/truck.controller");

module.exports = function(app) {
  app.post('/api/trucks',
    authJwt.verifyToken,
    authJwt.isDriver,
    controller.addTruck
  );

  app.get('/api/trucks/:id',
    authJwt.verifyToken,
    authJwt.isDriver,
    controller.getTruckById
  );

  app.put('/api/trucks/:id',
    authJwt.verifyToken,
    authJwt.isDriver,
    controller.updateTruck
  );

  app.get('/api/trucks',
    authJwt.verifyToken,
    authJwt.isDriver,
    controller.getTrucks
  );

  app.delete('/api/trucks/:id',
    authJwt.verifyToken,
    authJwt.isDriver,
    controller.deleteTruck
  );

  app.post('/api/trucks/:id/assign',
    authJwt.verifyToken,
    authJwt.isDriver,
    checkAssignedTrucks,
    controller.assignTruck
  );
}