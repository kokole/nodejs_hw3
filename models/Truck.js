const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TruckSchema = new Schema({
	created_by: String,
	assigned_to: String,
	type: {
		type: String,
		enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
	},
	status: {
		type: String,
		enum: ['OL', 'IS'],
	},
	created_date: {
		type: String,
		default: (new Date(Date.now())).toISOString(),
	},
})

module.exports = Truck = mongoose.model('Truck', TruckSchema);