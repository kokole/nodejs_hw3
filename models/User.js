const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
  },
	created_date: {
		type: String,
		default: (new Date(Date.now())).toISOString(),
	},
})

module.exports = User = mongoose.model('User', UserSchema);